<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>

<div class="container">
    <h1>Commandeer TCG Cards List</h1>
    <div class="row">
        <div class="col-sm-3" style="background:#EEE;">
            <form method="get" action="">
                <h3>Filters</h3>
                <div class="form-group">
                    <label class="control-label">Type</label>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="type[]" value="unit"<?php if (in_array('unit', $input['type'])) { echo ' checked'; } ?>>
                            Unit
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="type[]" value="event"<?php if (in_array('event', $input['type'])) { echo ' checked'; } ?>>
                            Event
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label">Faction</label>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="faction[]" value="Shangri-la"<?php if (in_array('Shangri-la', $input['faction'])) { echo ' checked'; } ?>>
                            Shangri-la
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="faction[]" value="Avalon"<?php if (in_array('Avalon', $input['faction'])) { echo ' checked'; } ?>>
                            Avalon
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="faction[]" value="Lemuria"<?php if (in_array('Lemuria', $input['faction'])) { echo ' checked'; } ?>>
                            Lemuria
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="faction[]" value="Heisenberg"<?php if (in_array('Heisenberg', $input['faction'])) { echo ' checked'; } ?>>
                            Heisenberg
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="faction[]" value="Brave Kingdom"<?php if (in_array('Brave Kingdom', $input['faction'])) { echo ' checked'; } ?>>
                            Brave Kingdom
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="faction[]" value="The Justice League"<?php if (in_array('The Justice League', $input['faction'])) { echo ' checked'; } ?>>
                            The Justice League
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="faction[]" value="Neutral"<?php if (in_array('Neutral', $input['faction'])) { echo ' checked'; } ?>>
                            Neutral
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $input['name'] }}" placeholder="Card Name">
                </div>

                <div class="form-group">
                    <label class="control-label" for="text">Text</label>
                    <input type="text" class="form-control" id="text" name="text" value="{{ $input['text'] }}" placeholder="Keyword to search in text">
                </div>

                <div class="form-group">
                    <label class="control-label" for="trait">Trait</label>
                    <input type="text" class="form-control" id="trait" name="trait" value="{{ $input['trait'] }}" placeholder="Trait (e.g. fox, pirate, arcane, etc.)">
                </div>

                <div class="form-group">
                    <label class="control-label">Command</label>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="command[]" value="0"<?php if (in_array('0', $input['command'])) { echo ' checked'; } ?>> 0
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="command[]" value="1"<?php if (in_array(1, $input['command'])) { echo ' checked'; } ?>> 1
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="command[]" value="2"<?php if (in_array(2, $input['command'])) { echo ' checked'; } ?>> 2
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="command[]" value="3"<?php if (in_array(3, $input['command'])) { echo ' checked'; } ?>> 3
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="command[]" value="4"<?php if (in_array(4, $input['command'])) { echo ' checked'; } ?>> 4
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="command[]" value="X"<?php if (in_array('X', $input['command'])) { echo ' checked'; } ?>> X
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label">Energy</label>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="energy[]" value="0"<?php if (in_array(0, $input['energy'])) { echo ' checked'; } ?>> 0
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="energy[]" value="1"<?php if (in_array(1, $input['energy'])) { echo ' checked'; } ?>> 1
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="energy[]" value="2"<?php if (in_array(2, $input['energy'])) { echo ' checked'; } ?>> 2
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="energy[]" value="3"<?php if (in_array(3, $input['energy'])) { echo ' checked'; } ?>> 3
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="energy[]" value="X"<?php if (in_array('X', $input['energy'])) { echo ' checked'; } ?>> X
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label">Filter by Unit Direction</label>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="direction_f" value="1"<?php if ($input['direction_f'] == 1) { echo ' checked'; } ?>> Front
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="direction_l" value="1"<?php if ($input['direction_l'] == 1) { echo ' checked'; } ?>> Left
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="direction_r" value="1"<?php if ($input['direction_r'] == 1) { echo ' checked'; } ?>> Right
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="direction_b" value="1"<?php if ($input['direction_b'] == 1) { echo ' checked'; } ?>> Back
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label">Filter by Unit Type</label>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="is_legendary" value="1"<?php if ($input['is_legendary'] == 1) { echo ' checked'; } ?>> Legendary
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="is_range" value="1"<?php if ($input['is_range'] == 1) { echo ' checked'; } ?>> Ranged Attack
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label">Filter by Event Type</label>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="is_ue" value="1"<?php if ($input['is_ue'] == 1) { echo ' checked'; } ?>> Ultimate Event
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="is_ongoing" value="1"<?php if ($input['is_ongoing'] == 1) { echo ' checked'; } ?>> Ongoing Event
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="is_planning" value="1"<?php if ($input['is_planning'] == 1) { echo ' checked'; } ?>> Planning Event
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="padding-right:50px;">
                            <input type="checkbox" name="is_defensive" value="1"<?php if ($input['is_defensive'] == 1) { echo ' checked'; } ?>> Defensive Event
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label">Set</label>
                    <?php foreach ($sets as $set) { ?>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="set[]" value="{{ $set->id }}"<?php if (in_array($set->id, $input['set'])) { echo ' checked'; } ?>> {{ $set->name }}
                            </label>
                        </div>
                    <?php } ?>
                </div>

                <div class="form-group">
                    <label class="control-label">Rarity</label>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="rarity[]" value="LG"<?php if (in_array('LG', $input['rarity'])) { echo ' checked'; } ?>> LG
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="rarity[]" value="RR"<?php if (in_array('RR', $input['rarity'])) { echo ' checked'; } ?>> RR
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="rarity[]" value="R"<?php if (in_array('R', $input['rarity'])) { echo ' checked'; } ?>> R
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="rarity[]" value="C"<?php if (in_array('C', $input['rarity'])) { echo ' checked'; } ?>> C
                        </label>
                    </div>
                </div>

<?php /*
                <div class="form-group type-unit">
                    <label class="col-sm-3 control-label" for="power">Power</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="power" name="power" value="">
                    </div>
                </div>

                <div class="form-group type-unit">
                    <label class="col-sm-3 control-label" for="health">Health</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="health" name="health" value="">
                    </div>
                </div>

                <div class="form-group type-unit">
                    <label class="col-sm-3 control-label">Direction</label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="direction_f" value="1">
                                Front
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="direction_l" value="1">
                                Left
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="direction_r" value="1">
                                Right
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="direction_b" value="1">
                                Back
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group type-unit">
                    <label class="col-sm-3 control-label">Unit Type</label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_legendary" value="1">
                                Legendary
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_range" value="1">
                                Range
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group type-event">
                    <label class="col-sm-3 control-label">Event Type</label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_planning" value="1">
                                Planning Event
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_defensive" value="1">
                                Defensive Event
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_ongoing" value="1">
                                Ongoing Event
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_ue" value="1">
                                Ultimate Event
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="code">Code</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="code" name="code" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="illust">Illust</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="illust" name="illust" value="">
                    </div>
                </div>
*/ ?>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </form>
        </div>

        <div class="col-sm-9">
            <?php if (!$cards->isEmpty()) { ?>
                <table class="cards-table">
                <?php foreach ($cards as $key => $card) { ?>
                    <tr class="single-line">
                        <td rowspan="4" class="td-img">
                            <?php if (!empty($card->image)) { ?><img src="{{ $card->image }}"><?php } ?>
                        </td>
                        <td colspan="5">
                            <strong>{{ $card->name }}</strong>
                        </td>
                        <td style="text-align:right;" width="20%">
                            {{ $card->trait }}
                        </td>
                    </tr>
                    <tr class="single-line">
                        <td colspan="2" style="border-right:none;">
                            {{ $card->faction }}
                        </td>
                        <td colspan="4" style="text-align:right; border-left:none;">{{ $card->set_name }} ({{ $card->rarity }})<?php /* (CMD03 001/084 RR) */ ?></td>
                    </tr>
                    <tr class="single-line">
                        <?php if ($card->type == 'unit') { ?>
                            <td width="7%" style="text-align:center;">{{ $card->command }} C</td>
                            <td width="7%" style="text-align:center;">{{ $card->energy }} E</td>
                            <td width="15%">Power: {{ $card->power }}</td>
                            <td width="15%">Health: {{ $card->health }}</td>
                            <td colspan="2">Direction: {{ $card->direction }}</td>
                        <?php } elseif ($card->type == 'event') { ?>
                            <td width="7%">{{ $card->command }} C</td>
                            <td width="7%">{{ $card->energy }} E</td>
                            <td colspan="4">&nbsp;</td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <p><?php echo nl2br($card->text_en) ?></p>
                            <p><?php echo nl2br($card->text_th) ?></p>
                        </td>
                    </tr>
                <?php } ?>
                </table>
            <?php } else { ?>
                <div class="alert alert-info">
                    <?php echo empty($input['set']) ? 'Please select condition to search card' : 'No query result.' ; ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>


<!--
<script>
$(function(){
    $('input[name=type]').change(function(){
        if ($(this).val() == 'unit') {
            $('.type-event').hide();
            $('.type-unit').show();
        } else if ($(this).val() == 'event') {
            $('.type-unit').hide();
            $('.type-event').show();
        }
    });
});
</script>
<style>
.type-unit, .type-event { display:none; }
</style>
-->

<style type="text/css">
.cards-table .single-line { height:20px; }
.cards-table img, .td-img { width:150px; }
.cards-table tr td { padding:10px; border:1px solid #999; }
</style>

</body>
</html>