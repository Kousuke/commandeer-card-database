@extends('layouts.admin')

@section('title')
    Add Card Data
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Add Card</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" method="post" action="{{ route('admin.cards.create') }}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="box-body">
          <div class="row">

            <div class="col-xs-9">

              <div class="form-group">
                <label for="name">Card Name</label>
                <input type="text" class="form-control" id="name" name="name" value="">
              </div>

              <div class="form-group">
                <label for="text">Text EN</label>
                <textarea class="form-control" id="text_en" name="text" rows="3" cols="60"></textarea>
              </div>

              <div class="form-group">
                <label for="text">Text TH</label>
                <textarea class="form-control" id="text_en" name="text" rows="3" cols="60"></textarea>
              </div>

              <div class="row">
                <div class="col-xs-4">
                  <div class="form-group">
                    <label for="rarity">Rarity</label>
                    <input type="text" class="form-control" id="rarity" name="rarity" value="">
                  </div>
                </div>
              </div><!-- /.row -->

              <div class="row">
                <div class="col-xs-2">
                  <div class="form-group">
                    <label for="command">Command</label>
                    <input type="text" class="form-control" id="command" name="command" value="">
                  </div>
                </div>
                <div class="col-xs-offset-2 col-xs-2">
                  <div class="form-group">
                    <label for="energy">Energy</label>
                    <input type="text" class="form-control" id="energy" name="energy" value="">
                  </div>
                </div>
              </div>

              <div class="row type-unit">
                <div class="col-xs-4">
                  <div class="form-group">
                    <label for="power">Power</label>
                    <input type="text" class="form-control" id="power" name="power" value="">
                  </div>
                </div>
                <div class="col-xs-4">
                  <div class="form-group">
                    <label for="health">Health</label>
                    <input type="text" class="form-control" id="health" name="health" value="">
                  </div>
                </div>
              </div>

              <div class="row type-unit">
                <div class="col-xs-8">
                  <div class="form-group">
                    <label for="trait">Trait</label>
                    <input type="text" class="form-control" id="trait" name="trait" value="">
                  </div>
                </div>
              </div><!-- /.row -->

              <div class="row type-unit">
                <div class="col-xs-2">
                  <div class="form-group">
                    <label>Direction</label>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="direction_f" value="1">
                        Front
                      </label>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="direction_l" value="1">
                        Left
                      </label>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="direction_r" value="1">
                        Right
                      </label>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="direction_b" value="1">
                        Back
                      </label>
                    </div>
                  </div>
                </div>
                <div class="col-xs-2">
                  <div class="form-group">
                    <label>MetaData</label>
                    <div class="checkbox">
                      <label>
                          <input type="checkbox" name="is_legendary" value="1">
                          Legendary
                      </label>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="is_range" value="1">
                        Range
                      </label>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row type-event">
                <div class="col-xs-3">
                  <div class="form-group">
                    <label>Event Type</label>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_planning" value="1">
                            Planning Event
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_defensive" value="1">
                            Defensive Event
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_ongoing" value="1">
                            Ongoing Event
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_ue" value="1">
                            Ultimate Event
                        </label>
                    </div>
                  </div>
                </div>
              </div><!-- /.row -->

              <div class="row">
                <div class="col-xs-4">
                  <div class="form-group">
                    <label for="code">Code</label>
                    <input type="text" class="form-control" id="code" name="code" value="">
                  </div>
                </div>

                <div class="col-xs-8">
                  <div class="form-group">
                    <label for="illust">Illust</label>
                    <input type="text" class="form-control" id="illust" name="illust" value="">
                  </div>
                </div>
              </div><!-- /.row -->

            </div><!-- /.col-xs-9 -->

            <div class="col-xs-3">
              <div class="form-group">
                <label>Type</label>
                <div class="radio">
                  <label>
                      <input type="radio" name="type" value="unit">
                      Unit
                  </label>
                </div>
                <div class="radio">
                  <label>
                      <input type="radio" name="type" value="event">
                      Event
                  </label>
                </div>
              </div>

              <div class="form-group">
                <label>Faction</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="faction" value="Shangri-la">
                        Shangri-la
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="faction" value="Avalon">
                        Avalon
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="faction" value="Lemuria">
                        Lemuria
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="faction" value="Heisenberg">
                        Heisenberg
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="faction" value="Brave Kingdom">
                        Brave Kingdom
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="faction" value="DC">
                        DC
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="faction" value="Neutral">
                        Neutral
                    </label>
                </div>
              </div>
            </div><!-- /.col-xs-3 -->

          </div><!-- /.row -->



          <?php /*
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
          <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" id="exampleInputFile">
            <p class="help-block">Example block-level help text here.</p>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox"> Check me out
            </label>
          </div>
          */ ?>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div><!-- /.box -->

  </div>
</div>
@endsection

@section('footer-script')
<script>
$(function(){
    $('input[name=type]').change(function(){
        if ($(this).val() == 'unit') {
            $('.type-event').hide();
            $('.type-unit').show();
        } else if ($(this).val() == 'event') {
            $('.type-unit').hide();
            $('.type-event').show();
        }
    });
});
</script>
<style>
.type-unit, .type-event { display:none; }
</style>
@endsection