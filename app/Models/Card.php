<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentTaggable\Taggable;

class Card extends Model
{
    use Taggable;

    // public function sets()
    // {
    //     return $this->belongsToMany('App\Models\Set');
    // }

    public function set()
    {
        return $this->belongsTo('App\Models\Set');
    }

    public function getDirectionAttribute()
    {
        $arr = [];
        if ($this->direction_f) {
            $arr[] = 'F';
        }
        if ($this->direction_l) {
            $arr[] = 'L';
        }
        if ($this->direction_r) {
            $arr[] = 'R';
        }
        if ($this->direction_b) {
            $arr[] = 'B';
        }
        return implode(', ', $arr);
    }

    public function getSetNameAttribute()
    {
        return $this->set->name;
    }
}
