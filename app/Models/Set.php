<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Set extends Model
{
    public $timestamps = false;

    // public function cards()
    // {
    //     return $this->belongsToMany('App\Models\Card');
    // }

    public function cards()
    {
        return $this->hasMany('App\Models\Card');
    }
}
