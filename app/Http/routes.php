<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'as'   => 'index',
    'uses' => 'HomeController@getIndex'
]);

// Authentication routes...
Route::group([
    'as'        => 'admin.auth.',
    'prefix'    => 'admin/auth',
    'namespace' => 'Admin\Auth'
], function () {
    Route::get('/login', [
        'as'   => 'login',
        'uses' => 'AuthController@getLogin'
    ]);
    Route::post('/login', [
        'as'   => 'login.post',
        'uses' => 'AuthController@postLogin'
    ]);
    Route::get('/logout', [
        'as'   => 'logout',
        'uses' => 'AuthController@getLogout'
    ]);
    // Route::get('/register', [
    //     'as'   => 'register',
    //     'uses' => 'AuthController@getRegister'
    // ]);
    // Route::post('/register', [
    //     'as'   => 'register.post',
    //     'uses' => 'AuthController@postRegister'
    // ]);
});



Route::group([
    'as'         => 'admin.',
    'prefix'     => 'admin',
    'namespace'  => 'Admin',
    'middleware' => 'auth'
], function() {
    Route::get('/', [
        'as'   => 'index',
        'uses' => 'AdminController@getIndex'
    ]);

    Route::group([
        'as'         => 'cards.',
        'prefix'     => '/cards',
    ], function() {
        Route::get('/create', [
            'as'   => 'create',
            'uses' => 'CardsController@getCreate'
        ]);
        Route::post('/create', [
            'as'   => 'create.post',
            'uses' => 'CardsController@postCreate'
        ]);
    });
});