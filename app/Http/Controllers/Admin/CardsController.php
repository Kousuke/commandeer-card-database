<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller as BaseController;

class CardsController extends BaseController
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function getIndex()
    {
        // echo 'admin index';
        // return view('admin.index');
    }

    public function getCreate()
    {
        return view('admin.cards.create');
    }

    public function postCreate()
    {
        // Include : Set, Image
        // Exclude : code, Illust

        $card           = new \App\Models\Card;
        $card->code     = request()->input('code');
        $card->rarity   = request()->input('rarity');
        $card->name     = request()->input('name');
        $card->text_en  = request()->input('text_en');
        $card->text_th  = request()->input('text_th');
        $card->illust   = request()->input('illust');
        $card->faction = request()->input('faction');
        $card->command  = request()->input('command');
        $card->energy   = request()->input('energy');
        $card->type     = request()->input('type');
        if ($card->type == 'unit') {
            $card->power        = (int) request()->input('power');
            $card->health       = (int) request()->input('health');
            $card->direction_f  = (int) request()->input('direction_f');
            $card->direction_l  = (int) request()->input('direction_l');
            $card->direction_r  = (int) request()->input('direction_r');
            $card->direction_b  = (int) request()->input('direction_b');
            $card->is_range     = (int) request()->input('is_range');
            $card->is_legendary = (int) request()->input('is_legendary');
        } elseif ($card->type == 'event') {
            $card->is_ue        = (int) request()->input('is_ue');
            $card->is_planning  = (int) request()->input('is_planning');
            $card->is_defensive = (int) request()->input('is_defensive');
            $card->is_ongoing   = (int) request()->input('is_ongoing');
        }
        $card->save();

        $traits = request()->input('trait');
        $card->tag($traits);
    }
}
