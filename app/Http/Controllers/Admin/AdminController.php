<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller as BaseController;
// use Illuminate\Foundation\Bus\DispatchesJobs;
// use Illuminate\Foundation\Validation\ValidatesRequests;
// use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
// use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class AdminController extends BaseController
{
    // use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function getIndex()
    {
        // echo 'admin index';
        return view('admin.index');
    }
}
