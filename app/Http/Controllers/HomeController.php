<?php

namespace App\Http\Controllers;

// use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
// use Illuminate\Foundation\Validation\ValidatesRequests;
// use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
// use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class HomeController extends BaseController
{
    // use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    // CMD01, CMD02, 4 Starters, DC Starter, Promo
    // 4 Starters II, BraveKingdom Starter

    public function getIndex()
    {
        \DB::enableQueryLog();
        $data = [];
        $query = (new \App\Models\Card)->newQuery()->with(['set']);

        // $latestSet = \App\Models\Set::orderBy('id', 'desc')->first()->id;

        $input = [
            'type'         => ['unit', 'event'],
            'faction'      => ['Shangri-la', 'Avalon', 'Lemuria', 'Heisenberg', 'Brave Kingdom', 'The Justice League', 'Neutral'],
            'name'         => '',
            'text'         => '',
            'trait'        => '',
            'command'      => [0, 1, 2, 3, 4, 'X'],
            'energy'       => [0, 1, 2, 3, 'X'],
            // 'set'          => \App\Models\Set::lists('id')->toArray(),
            // 'set'          => [$latestSet],
            'set'          => [],
            'rarity'       => ['LG', 'RR', 'R', 'C'],
            'direction_f'  => 0,
            'direction_l'  => 0,
            'direction_r'  => 0,
            'direction_b'  => 0,
            'is_legendary' => 0,
            'is_range'     => 0,
            'is_ue'        => 0,
            'is_planning'  => 0,
            'is_defensive' => 0,
            'is_ongoing'   => 0,
        ];

        $input = array_merge($input, request()->all());

        $query->whereIn('type', $input['type'])
            ->whereIn('set_id', $input['set'])
            ->whereIn('faction', $input['faction'])
            ->whereIn('command', $input['command'])
            ->whereIn('energy', $input['energy'])
            ->whereIn('rarity', $input['rarity']);

        if (!empty($input['name'])) {
            $query->where('name', 'like', '%'.$input['name'].'%');
        }

        if (!empty($input['text'])) {
            $query->where(function($q) use($input) {
                $q->where('text_en', 'like', '%'.$input['text'].'%')->orWhere('text_th', 'like', '%'.$input['text'].'%');
            });
        }

        if (!empty($input['trait'])) {
            $query->where('trait', 'like', '%'.$input['trait'].'%');
        }

        $cards = $query->get();

        // Filter
        $filteredCards = $cards->filter(function($card) use($input){
            if ($card->type == 'unit') {
                if ($input['direction_f'] == 1 && $card->direction_f != 1) {
                    return FALSE;
                }
                if ($input['direction_l'] == 1 && $card->direction_l != 1) {
                    return FALSE;
                }
                if ($input['direction_r'] == 1 && $card->direction_r != 1) {
                    return FALSE;
                }
                if ($input['direction_b'] == 1 && $card->direction_b != 1) {
                    return FALSE;
                }
                if ($input['is_legendary'] == 1 && $card->is_legendary != 1) {
                    return FALSE;
                }
                if ($input['is_range'] == 1 && $card->is_range != 1) {
                    return FALSE;
                }
            } elseif ($card->type == 'event') {
                if ($input['is_ue'] == 1 && $card->is_ue != 1) {
                    return FALSE;
                }
                if ($input['is_ongoing'] == 1 && $card->is_ongoing != 1) {
                    return FALSE;
                }
                if ($input['is_planning'] == 1 && $card->is_planning != 1) {
                    return FALSE;
                }
                if ($input['is_defensive'] == 1 && $card->is_defensive != 1) {
                    return FALSE;
                }
            }
            return TRUE;
        });

        $data['cards'] = $filteredCards;
        $data['input'] = $input;
        $data['sets']  = \App\Models\Set::all();

        return view('index', $data);
    }
}
